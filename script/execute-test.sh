#!/bin/bash
set -euo pipefail

printf "\n=====================================================================================\n"
printf "KOBITON EXECUTE TEST PLUGIN"
printf "\n=====================================================================================\n\n"

TARGET_OS="${RUNNER_OS:-"linux"}"
echo "OS: ${TARGET_OS}"
APP_TO_RUN="app_${TARGET_OS}"

wget "https://gitlab.com/sonhmle/test-gitlab-ci/-/raw/main/execute-test/app-to-run/${APP_TO_RUN}"
chmod +x ${APP_TO_RUN}

./${APP_TO_RUN}